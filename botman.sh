if (( $# == 0 ));
then
	echo "Utilidad para añadir, ver, retirar y cambiar la API Key de los bots de la plataforma TesTA."
	echo "Uso: ./botman.sh <l[ista]|a[ñadir]|b[orrar]|ac[tualizar]>";
	exit;
fi
docker compose exec platform java -Djava.util.logging.SimpleFormatter.format='%5$s%6$s%n' -jar TesTA.jar --botman=$1
