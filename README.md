# Habla con TesTA - Docker
Este proyecto es la estructura necesaria para poder ejecutar el proyecto de [Habla con TesTA](https://gitlab.com/fmpg0003/testa) usando Docker.

## Instalación
1. Descarga el proyecto en formato .zip, o clónalo.
2. Edita el archivo `config/config.testa.example` editándolo con las opciones de configuración necesarias de la plataforma. Todas las opciones de configuración están explicadas claramente, en inglés, dentro del fichero de configuración.
3. Renombra `config/config.testa.example` a `config/config.testa`.
4. Si necesitas cambiar el puerto por defecto, en lugar de modificarlo por medio del archivo `config/config.testa`, modifica `docker-compose.yml` (Línea 28), cambiando el **primer** puerto por la opción que necesites (Por ejemplo, `10000:3574` para abrir el puerto 10000 de tu máquina a la API para bots).
5. Ejecuta el fichero `build.bat` (Windows) o `build.sh` (Linux) para construir el contenedor de la aplicación. Cuando se acabe de construir, la aplicación se iniciará automáticamente.

## Iniciar y parar la plataforma
Puede pararse la plataforma automáticamente por medio de los comandos básicos de Docker o usando los ficheros `start` y `stop` (.sh para Linux, .bat para Windows).

## BotManager
Para acceder al BotManager de la plataforma, puede hacerse por medio de los comandos básicos de Docker Compose o usando directamente el script `botman` de la plataforma (.sh para Linux, .bat para Windows). La sintaxis es:
```
./botman.<sh|bat> <argumento>
```
Donde `argumento` es uno de los [argumentos del BotManager](https://gitlab.com/fmpg0003/TesTA#ejecuci%C3%B3n-del-programa).
Una sintaxis válida de ejemplo sería:
```
./botman.sh l
```
Para mostrar la lista de bots de la plataforma en un sistema Linux.