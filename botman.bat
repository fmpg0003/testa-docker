@echo off
IF "%1"=="" GOTO usage
docker compose exec platform java -Djava.util.logging.SimpleFormatter.format=%%5$s%%6$s%%n -jar TesTA.jar --botman=%1
if %errorlevel% NEQ 0 GOTO err
exit
:usage
	echo Utilidad para a�adir, ver, retirar y cambiar la API Key de los bots de la plataforma TesTA.
	echo Uso: ./botman.bat ^<l[ista]^|a[�adir]^|b[orrar]^|ac[tualizar]^>
	exit
	
:err
	echo Ha ocurrido un error ejecutando el comando. Es posible que el contenedor est� apagado. Ejecuta el fichero start.bat antes.

