CREATE DATABASE  IF NOT EXISTS `testa` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `testa`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 192.168.1.10    Database: testa
-- ------------------------------------------------------
-- Server version	5.5.5-10.11.6-MariaDB-0+deb12u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bots`
--

DROP TABLE IF EXISTS `bots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiKey` varchar(64),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_bots_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bots_conversaciones_finalizadas_no_enviadas`
--

DROP TABLE IF EXISTS `bots_conversaciones_finalizadas_no_enviadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bots_conversaciones_finalizadas_no_enviadas` (
  `id_bot` int(11) NOT NULL,
  `id_conversacion` varchar(64) NOT NULL,
  `isBot` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_bot`,`id_conversacion`),
  KEY `fk_bots_conversaciones_finalizadas_no_enviadas_id_bot_idx` (`id_bot`),
  KEY `fk_bots_conversaciones_finalizadas_no_enviadas_id_conversac_idx` (`id_conversacion`),
  CONSTRAINT `fk_bots_conversaciones_finalizadas_no_enviadas_id_bot` FOREIGN KEY (`id_bot`) REFERENCES `bots` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_bots_conversaciones_finalizadas_no_enviadas_id_conversacion` FOREIGN KEY (`id_conversacion`) REFERENCES `conversaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bots_conversaciones_finalizadas_no_enviadas`
--

LOCK TABLES `bots_conversaciones_finalizadas_no_enviadas` WRITE;
/*!40000 ALTER TABLE `bots_conversaciones_finalizadas_no_enviadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `bots_conversaciones_finalizadas_no_enviadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bots_mensajes_no_enviados`
--

DROP TABLE IF EXISTS `bots_mensajes_no_enviados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bots_mensajes_no_enviados` (
  `id_bot` int(11) NOT NULL,
  `id_conversacion` varchar(64) NOT NULL,
  `mensaje` varchar(4096) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`timestamp`,`id_bot`,`id_conversacion`),
  KEY `fk_bots_mensajes_no_enviados_conversacion_idx` (`id_conversacion`),
  KEY `fk_bots_mensajes_no_enviados_id_idx` (`id_bot`),
  CONSTRAINT `fk_bots_mensajes_no_enviados_conversacion` FOREIGN KEY (`id_conversacion`) REFERENCES `conversaciones` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_bots_mensajes_no_enviados_id` FOREIGN KEY (`id_bot`) REFERENCES `bots` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bots_mensajes_no_enviados`
--

LOCK TABLES `bots_mensajes_no_enviados` WRITE;
/*!40000 ALTER TABLE `bots_mensajes_no_enviados` DISABLE KEYS */;
/*!40000 ALTER TABLE `bots_mensajes_no_enviados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversaciones`
--

DROP TABLE IF EXISTS `conversaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conversaciones` (
  `id` varchar(64) NOT NULL,
  `interlocutor1` int(11) DEFAULT NULL,
  `interlocutor2` int(11) DEFAULT NULL,
  `timestamp_inicio` bigint(20) NOT NULL DEFAULT 0,
  `timestamp_fin` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_conversaciones_interlocutor1_idx` (`interlocutor1`),
  KEY `fk_conversaciones_interlocutor2_idx` (`interlocutor2`),
  CONSTRAINT `fk_conversaciones_interlocutor1` FOREIGN KEY (`interlocutor1`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_conversaciones_interlocutor2` FOREIGN KEY (`interlocutor2`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversaciones_encuesta`
--

DROP TABLE IF EXISTS `conversaciones_encuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conversaciones_encuesta` (
  `id_conversacion` varchar(64) NOT NULL,
  `interlocutor` int(11) DEFAULT NULL,
  `otro_interlocutor_era_bot` tinyint(4) DEFAULT NULL,
  KEY `fk_conversaciones_encuesta_interlocutor_idx` (`interlocutor`),
  KEY `fk_conversaciones_encuesta_id_encuesta_idx` (`id_conversacion`),
  CONSTRAINT `fk_conversaciones_encuesta_id_encuesta` FOREIGN KEY (`id_conversacion`) REFERENCES `conversaciones` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_conversaciones_encuesta_interlocutor` FOREIGN KEY (`interlocutor`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `conversaciones_mensajes`
--

DROP TABLE IF EXISTS `conversaciones_mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conversaciones_mensajes` (
  `id_conversacion` varchar(64) NOT NULL,
  `id_interlocutor` int(11) DEFAULT NULL,
  `mensaje` varchar(4096) NOT NULL,
  `timestamp` bigint(20) NOT NULL DEFAULT 0,
  KEY `fk_conversaciones_mensajes_id_conversacion_idx` (`id_conversacion`),
  KEY `fk_conversaciones_mensajes_interlocutor_idx` (`id_interlocutor`),
  CONSTRAINT `fk_conversaciones_mensajes_id_conversacion` FOREIGN KEY (`id_conversacion`) REFERENCES `conversaciones` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_conversaciones_mensajes_interlocutor` FOREIGN KEY (`id_interlocutor`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `encuestas_complete_view`
--

DROP TABLE IF EXISTS `encuestas_complete_view`;
/*!50001 DROP VIEW IF EXISTS `encuestas_complete_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `encuestas_complete_view` AS SELECT 
 1 AS `id_conversacion`,
 1 AS `interlocutor`,
 1 AS `otro_interlocutor_era_bot`,
 1 AS `otro_interlocutor_type`,
 1 AS `timestamp_inicio`,
 1 AS `timestamp_fin`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `humans`
--

DROP TABLE IF EXISTS `humans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `humans` (
  `id` int(11) NOT NULL,
  `chat_id` bigint(20) NOT NULL,
  `lgpd` tinyint(4) NOT NULL,
  `conversation_survey` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `using_telegram_name` tinyint(4) DEFAULT 0,
  `changing_username` tinyint(4) DEFAULT 0,
  `in_matchmaking_msg_id` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_humans_conversation_survey` (`conversation_survey`),
  CONSTRAINT `fk_humans_conversation_survey` FOREIGN KEY (`conversation_survey`) REFERENCES `conversaciones` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_humans_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `type` enum('BOT','USER') DEFAULT NULL,
  `experience` bigint(20) DEFAULT 0,
  `join_timestamp` bigint(20) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `encuestas_complete_view`
--

/*!50001 DROP VIEW IF EXISTS `encuestas_complete_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `encuestas_complete_view` AS select `ce`.`id_conversacion` AS `id_conversacion`,`ce`.`interlocutor` AS `interlocutor`,`ce`.`otro_interlocutor_era_bot` AS `otro_interlocutor_era_bot`,`u`.`type` AS `otro_interlocutor_type`,`c`.`timestamp_inicio` AS `timestamp_inicio`,`c`.`timestamp_fin` AS `timestamp_fin` from ((`conversaciones` `c` join `conversaciones_encuesta` `ce`) join `user` `u`) where `ce`.`id_conversacion` = `c`.`id` and (`u`.`id` <> `ce`.`interlocutor` and `u`.`id` = `c`.`interlocutor1` or `u`.`id` <> `ce`.`interlocutor` and `u`.`id` = `c`.`interlocutor2`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-24  3:03:23
