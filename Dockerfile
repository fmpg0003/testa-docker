FROM alpine/git AS git
RUN git clone https://gitlab.com/fmpg0003/TesTA.git

FROM jelastic/maven:3.9.5-openjdk-21 AS build
COPY --from=git /git/TesTA /usr/src/app/TesTA
RUN mvn -f /usr/src/app/TesTA/pom.xml clean package

FROM openjdk:21-ea-33-jdk
COPY --from=build /usr/src/app/TesTA/target/TesTA-*-jar-with-dependencies.jar /usr/src/app/TesTA.jar
WORKDIR /usr/src/app
CMD ["mkdir","logs"]
CMD ["java","-Djava.util.logging.config.file=./logging.properties","-jar","TesTA.jar"]
